const headers = {
  'Content-Type': 'application/json',
  Accept: 'application/json'
};

const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
};

const parseJSON = (response) => response.json();

export const get = (path) => (
  fetch(`https://runkit.io/heltonvalentini/582c94eca07eb10014912576/branches/master/${path}`, { headers })
  .then(checkStatus)
  .then(parseJSON)
);
