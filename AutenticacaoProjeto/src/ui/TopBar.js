import React from 'react';
import {
  View,
} from 'react-native';

export const TopBar = (props) => (
  <View style={[styles.container, props.style]}>
    {props.children}
  </View>
);

TopBar.propTypes = {
  children: React.PropTypes.node,
  style: React.PropTypes.number,
};

const styles = {
  container: {
    height: 60,
    backgroundColor: '#5B8DF3',
    justifyContent: 'center',
    padding: 10,
  }
};
