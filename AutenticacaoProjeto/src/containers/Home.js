import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableNativeFeedback
} from 'react-native';
import { connect } from 'react-redux';
import {
  fetchUser
} from '../actions/actions.js';
import { TopBar } from '../ui/TopBar.js';

export class Container extends Component {
  componentDidMount() {
    this.props.fetchUser(this.props.userData.documentNumber, 'blabla');
  }
  render() {
    return (
      <View>
        <TopBar>
          <TouchableNativeFeedback onPress={() => this.props.navigator.push({ index: 0 })}>
            <Text style={styles.text}>Voltar</Text>
          </TouchableNativeFeedback></TopBar>
        <View style={styles.container}>
          <Text style={styles.title}>{`Bem vindo ${this.props.client.customerName}!`}</Text>
        </View>
      </View>
    );
  }
}

Container.propTypes = {
  fetchUser: React.PropTypes.func,
  navigator: React.PropTypes.object,
  userData: React.PropTypes.object,
  client: React.PropTypes.object
};

const mapStateToProps = (state) => ({
  client: state.client,
  userData: state.login
});

const mapActionToProps = (dispatch) => ({
  fetchUser(clientDocument, mockType) {
    dispatch(fetchUser(clientDocument, mockType));
  }
});
export const Home = connect(mapStateToProps, mapActionToProps)(Container);

const styles = {
  text: {
    color: '#fff',
    fontSize: 16,
  },
  title: {
    fontSize: 22
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  }
};
