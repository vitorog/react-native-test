import React from 'react';
import { Main } from './containers/Main.js';
import { Home } from './containers/Home.js';
import { ActivateClient } from './containers/ActivateClient.js';
import { Provider } from 'react-redux';
import { store } from './store';
import { Navigator } from 'react-native';
export const Root = () => (
  <Provider store={store}>
    <Navigator
      initialRoute={{ index: 0 }}
      renderScene={(route, navigator) => {
        if (route.index === 1) {
          return (<Home
            navigator={navigator}
          />
          );
        }
        if (route.index === 2) {
          return (<ActivateClient navigator={navigator} />);
        }
        return (<Main
          navigator={navigator}
        />);
      }}
    />
  </Provider>
);
