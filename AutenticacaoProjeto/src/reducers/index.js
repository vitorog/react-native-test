import { login } from './login';
import { ui } from './ui';
import { client } from './client';
import { combineReducers } from 'redux';

export default combineReducers({
  login,
  ui,
  client
});
