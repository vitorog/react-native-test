import * as types from '../actions/types';

const handleResponse = (state, payload) => {
  if (payload.errorCode === 0) {
    if (payload.clientStatus === 'ACTIVE') {
      return {
        ...state,
        message: 'Bem vindo!',
        showLoading: false
      }
    }
    if (payload.clientStatus === 'WAITING_ACTIVATION') {
      return {
        ...state,
        message: 'Usuário esperando ativação.',
        showMessage: true,
        showLoading: false
      }
    }
    if (payload.clientStatus === 'INACTIVE') {
      return {
        ...state,
        message: 'Opa, faz tempo que não entra em? Seu usuário está inativo...',
        showLoading: false
      }
    }
    if (payload.clientStatus === 'BLOCKED') {
      return {
        ...state,
        message: 'Seu usuário está bloqueado!',
        showLoading: false
      }
    }
  }
  return {
    ...state,
    message: payload.errorDesc,
    showLoading: false
  };
}
export const ui = (state = { showAnotherLoading: false, showMessage: false, showLoading:false, message: '' }, { type, payload }) => {
  switch (type) {
    case types.PRE_LOGIN:
      return {
        ...state,
        showLoading: true
      };
    case types.LOGIN_COMPLETE:
      return handleResponse(state, payload);
    case types.LOGIN_FB_COMPLETE:
      return handleResponse(state, payload);
    case types.ACTIVATE_USER:
      return {
        ...state,
        showAnotherLoading: true
      };
    case types.ACTIVATE_USER_COMPLETE:
      return {
        ...state,
        showAnotherLoading: false
      };
    default:
      return state;
  }
};
