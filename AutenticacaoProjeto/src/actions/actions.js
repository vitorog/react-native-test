import { createAction } from 'redux-actions';
import * as types from './types';
import * as api from '../services/api.js';
import FBSDK from 'react-native-fbsdk';
const {
  GraphRequest,
  GraphRequestManager
} = FBSDK;
export const login = (username, password, navigator) => (
  dispatch => {
    dispatch(createAction(types.PRE_LOGIN)(true));
    api
    .get(`login?username=${username}&password=${password}`)
    .then(response => {
      dispatch(createAction(types.LOGIN_COMPLETE)(response));
      if (response.errorCode === 0) {
        if (response.clientStatus !== 'WAITING_ACTIVATION') {
          navigator.push({ index: 1 });
        }
      }
    }
    );
  }
);

export const fetchUser = (clientDocument, mockType) => (
  dispatch => {
    api
    .get(`customer?document=${clientDocument}&mockType=${mockType}`)
    .then(response => dispatch(createAction(types.FETCH_USER_COMPLETE)(response)));
  }
);

export const activateUser = (clientDocument) => (
  dispatch => {
    dispatch(createAction(types.ACTIVATE_USER)(true));
    api
    .get(`activate?document=${clientDocument}&requestType=DOCUMENT`)
    .then(response => dispatch(createAction(types.ACTIVATE_USER_COMPLETE)(response)));
  }
);

export const getUser = (mockType, navigator) => (
  dispatch => {
    const infoRequest = new GraphRequest(
      '/me',
      null,
      (error, result) => {
        if (error) {
          alert(error);
        }
        api
        .get(`login-fb?facebookId=${result.id}&requestType=FACEBOOK&mockType=${mockType}`)
        .then((response) => {
          dispatch(createAction(types.LOGIN_FB_COMPLETE)(response));
          if (response.clientStatus !== 'WAITING_ACTIVATION') {
            navigator.push({ index: 1 });
          }
        });
      }
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  }
);
