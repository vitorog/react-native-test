import * as types from '../actions/types';

export const login = (state = {}, { type, payload }) => {
  switch (type) {
    case types.LOGIN_COMPLETE:
      return payload;
    case types.LOGIN_FB_COMPLETE:
      return payload;
    default:
      return state;
  }
};
