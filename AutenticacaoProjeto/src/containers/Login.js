import React, { Component } from 'react';
import {
      Alert,
      StyleSheet,
      TextInput,
      Image,
      View,
      TouchableNativeFeedback,
      TouchableOpacity,
      ActivityIndicator,
      Text
} from 'react-native';
import logo from '../../res/logo.png';
import LinearGradient from 'react-native-linear-gradient';
import FBSDK from 'react-native-fbsdk';
const {
  LoginButton
} = FBSDK;

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
      user: {}
    };
    this.login = this.login.bind(this);
    this.handleUserTextChange = this.handleUserTextChange.bind(this);
    this.handlePasswordTextChange = this.handlePasswordTextChange.bind(this);
    this.showMessage = this.showMessage.bind(this);
  }

  handleUserTextChange({ nativeEvent }) {
    const { text } = nativeEvent;
    this.setState({ userName: text });
  }

  handlePasswordTextChange({ nativeEvent }) {
    const { text } = nativeEvent;
    this.setState({ password: text });
  }

  login() {
    if (this.state.userName === '') {
      this.showAlert('Campo vazio', 'Por favor, informe o seu usuário.');
    } else if (this.state.password === '') {
      this.showAlert('Campo vazio', 'Por favor, informe sua senha.');
    } else {
      this.props.login(this.state.userName, this.state.password, this.props.navigator);
      this.setState({
        userName: '',
        password: ''
      });
    }
  }

  showAlert(title, message) {
    Alert.alert(
      title,
      message, [{ text: 'OK' }]
    );
  }

  showMessage(visible) {
    if (visible) {
      return (
        <View>
          <Text>
            {this.props.message}
          </Text>
          <TouchableNativeFeedback onPress={() => this.props.navigator.push({ index: 2 })}>
            <Text>Clique aqui para ativar</Text>
          </TouchableNativeFeedback>
        </View>
        );
    }
    return null;
  }
  render() {
    return (
      <LinearGradient
        colors={['#deeeee', '#3da4ab']}
        style={styles.container}
      >
        <View style={styles.header}>
          <Image source={logo} style={styles.logoStyle} />
        </View>
        <View style={styles.body}>
          <TextInput
            placeholder="Usuário"
            value={this.state.userName}
            onChange={this.handleUserTextChange}
          />
          <TextInput
            placeholder="Senha"
            secureTextEntry
            style={{ marginBottom: 15 }}
            value={this.state.password}
            onChange={this.handlePasswordTextChange}
          />
          <ActivityIndicator
            animating={this.props.showLoading}
            style={[styles.centering, { height: 80 }]}
            size="large"
          />
          <TouchableNativeFeedback onPress={this.login} style={styles.button} >
            <View
              style={{
                backgroundColor: '#01943f',
                justifyContent: 'center',
                alignItems: 'center' }}
            >
              <Text style={{ margin: 10, fontSize: 14, color: '#fff' }}>
                ENTRAR
              </Text>
            </View>
          </TouchableNativeFeedback>
          <View>
            {this.showMessage(this.props.showMessage)}
          </View>
          <View
            style={{
              marginTop: 10,
              marginBottom: 10 }}
          >
            <LoginButton
              publishPermissions={['publish_actions']}
              onLoginFinished={
                (error, result) => {
                  if (error) {
                    this.showAlert('Carona Legal', `login has error: ${result.error}`);
                  } else if (result.isCancelled) {
                    this.showAlert('Carona Legal', 'Login cancelado');
                  } else {
                    this.props.getUser('blabla', this.props.navigator);
                  }
                }
              }
              onLogoutFinished={() => this.showAlert('Carona Legal', 'Logout')}
            />
          </View>
          <View style={styles.otherBtn}>
            <TouchableOpacity style={styles.otherBtnText} >
              <View
                style={{
                  justifyContent: 'flex-end',
                  alignItems: 'center' }}
              >
                <Text style={{ margin: 10 }}>
                  ESQUECEU A SENHA?
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.otherBtnText} >
              <View
                style={{
                  justifyContent: 'flex-end',
                  alignItems: 'center' }}
              >
                <Text style={{ margin: 10 }}>
                  CADASTRE-SE
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </LinearGradient>
  );
  }
}
Login.propTypes = {
  showMessage: React.PropTypes.bool,
  showLoading: React.PropTypes.bool,
  message: React.PropTypes.string,
  login: React.PropTypes.func,
  getUser: React.PropTypes.func,
  userData: React.PropTypes.object,
  navigator: React.PropTypes.object
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logoStyle: {
    width: 220,
    height: 220,
  },
  header: {
    marginTop: 40,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    flex: 0.8,
    margin: 20
  },
  button: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 15,
  },
  otherBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 40
  },
  otherBtnText: {
    flex: 1
  }
});
