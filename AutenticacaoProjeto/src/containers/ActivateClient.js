import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableNativeFeedback,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import {
  activateUser
} from '../actions/actions.js';
import { TopBar } from '../ui/TopBar.js';

export class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clientDocument: ''
    };
    this.handleDocumentTextChange = this.handleDocumentTextChange.bind(this);
    this.activateUser = this.activateUser.bind(this);
  }
  handleDocumentTextChange({ nativeEvent }) {
    const { text } = nativeEvent;
    this.setState({ clientDocument: text });
  }
  activateUser() {
    this.props.activateUser(this.state.clientDocument);
  }
  render() {
    return (
      <View>
        <TopBar>
          <TouchableNativeFeedback onPress={() => this.props.navigator.push({ index: 0 })}>
            <Text style={styles.text}>Voltar</Text>
          </TouchableNativeFeedback></TopBar>
        <View style={styles.container}>
          <Text style={styles.title}>'Informe seu CPF/CNPJ'</Text>
        </View>
        <TextInput
          placeholder="CPF/CNPJ"
          value={this.state.clientDocument}
          onChange={this.handleDocumentTextChange}
          style={styles.body}
        />
        <ActivityIndicator
          animating={this.props.showAnotherLoading}
          style={[styles.centering, { height: 80 }]}
          size="large"
        />
        <TouchableNativeFeedback onPress={this.activateUser} style={styles.button} >
          <View
            style={{
              backgroundColor: '#01943f',
              justifyContent: 'center',
              alignItems: 'center' }}
          >
            <Text style={{ margin: 10, fontSize: 14, color: '#fff' }}>
              ATIVAR
            </Text>
          </View>
        </TouchableNativeFeedback>
      </View>
    );
  }
}

Container.propTypes = {
  showAnotherLoading: React.PropTypes.bool,
  activateUser: React.PropTypes.func,
  navigator: React.PropTypes.object
};

const mapStateToProps = (state) => ({
  showAnotherLoading: state.ui.showAnotherLoading,
  client: state.client
});

const mapActionToProps = (dispatch) => ({
  activateUser(clientDocument) {
    dispatch(activateUser(clientDocument));
  }
});

export const ActivateClient = connect(mapStateToProps, mapActionToProps)(Container);


const styles = {
  button: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 15,
  },
  text: {
    color: '#fff',
    fontSize: 16,
  },
  title: {
    fontSize: 22
  },
  body: {
    margin: 20
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  }
};
