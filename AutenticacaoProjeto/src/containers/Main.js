import React from 'react';
import { Login } from './Login.js';
// import { NavigationExperimental } from 'react-native';
import { connect } from 'react-redux';
import {
  login,
  getUser
} from '../actions/actions.js';

export const Container = (props) => (
  <Login
    showMessage={props.showMessage}
    showLoading={props.showLoading}
    message={props.message}
    login={props.login}
    getUser={props.getUser}
    userData={props.userData}
    navigator={props.navigator}
  />
);

Container.propTypes = {
  showMessage: React.PropTypes.bool,
  showLoading: React.PropTypes.bool,
  message: React.PropTypes.string,
  login: React.PropTypes.func,
  getUser: React.PropTypes.func,
  userData: React.PropTypes.object,
  navigator: React.PropTypes.object
};

const mapStateToProps = (state) => ({
  userData: state.login,
  showMessage: state.ui.showMessage,
  showLoading: state.ui.showLoading,
  message: state.ui.message
});

const mapActionToProps = (dispatch) => ({
  login(username, password, navigator) {
    dispatch(login(username, password, navigator));
  },
  getUser(mockType, navigator) {
    dispatch(getUser(mockType, navigator));
  }
});
export const Main = connect(mapStateToProps, mapActionToProps)(Container);
