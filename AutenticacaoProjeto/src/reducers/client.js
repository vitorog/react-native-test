import * as types from '../actions/types';

export const client = (state = {}, { type, payload }) => {
  switch (type) {
    case types.FETCH_USER_COMPLETE:
      return payload;
    case types.ACTIVATE_USER_COMPLETE:
      return payload;
    default:
      return state;
  }
};
